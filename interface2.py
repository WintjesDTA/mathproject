import sys, os
from PyQt5 import QtWidgets
import subprocess
import numpy as np
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import random

class Window(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtWidgets.QPushButton('Start Optimizing')
        self.button.clicked.connect(self.startSimulation)

        # Labels and textboxes
        self.t1 = QtWidgets.QLineEdit(self)
        self.t1.setFixedWidth(40)
        self.t1.setText("0.4")
        l1 = QtWidgets.QLabel()
        l1.setText("Volume fraction")


        self.t2 = QtWidgets.QLineEdit(self)
        self.t2.setFixedWidth(40)
        self.t2.setText("20")
        l2 = QtWidgets.QLabel()
        l2.setText("Size")


        self.t3 = QtWidgets.QLineEdit(self)
        self.t3.setFixedWidth(40)
        self.t3.setText("3")
        l3 = QtWidgets.QLabel()
        l3.setText("Penalty")




        # set the layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)

        hbox1 = QtWidgets.QHBoxLayout()
        hbox1.addStretch(1)
        hbox1.addWidget(l1)
        hbox1.addWidget(self.t1)
        layout.addLayout(hbox1)

        hbox2 = QtWidgets.QHBoxLayout()
        hbox2.addStretch(1)
        hbox2.addWidget(l2)
        hbox2.addWidget(self.t2)
        layout.addLayout(hbox2)

        hbox3 = QtWidgets.QHBoxLayout()
        hbox3.addStretch(1)
        hbox3.addWidget(l3)
        hbox3.addWidget(self.t3)
        layout.addLayout(hbox3)

        layout.addWidget(self.button)
        self.setLayout(layout)

    def startSimulation(self):
        ''' plot some random stuff '''
        # random data
        #
        dens = []
        volumefrac = self.t1.text()
        elements = self.t2.text()
        penalty = self.t3.text()
        sys.path.append('/usr/include/CoinIpopt/lib')
        subprocess.call(["./plate_main",volumefrac, elements, penalty])
        f = open('plate_main.out', 'r')
        for line in f:
            ls = line.split()
            dens.append(ls)
            #data = [random.random() for i in range(10)]
            temperature = [[float(column) for column in row] for row in dens]
        dens = np.matrix(temperature)

        f.close()

        
        temp = []
        f = open('plate_temp.out','r')
        for line in f:
            ls = line.split()
            temp.append(ls)
            #data = [random.random() for i in range(10)]
            temperature = [[float(column) for column in row] for row in temp]
        temp = np.matrix(temperature)
        #max = np.matrix.max(temp)
        #temp = temp/max
        #print(temp)
        #data = np.ma.masked_greater(temp, 1.0)
        # create an axis

        # discards the old graph
        self.figure.clear()
        ax = self.figure.add_subplot(2,1,1)
        fig = self.figure
        dens = np.array(dens)

        im = ax.pcolormesh(dens, cmap='Greys', edgecolors='black', linewidths=1, #YlOrRd of Greys
                           antialiased=True)
        fig.colorbar(im)

        ax.patch.set(hatch='xx', edgecolor='black')

        ax2 = self.figure.add_subplot(2,1,2)
        temp = np.array(temp)
        im = ax2.pcolormesh(temp, cmap='hot', edgecolors='black', linewidths=1, #YlOrRd of Greys
                           antialiased=True)
        fig.colorbar(im)

        #plt.show()

        # plot data
        #ax.plot(data, '*-')

        # refresh canvas
        self.canvas.draw()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
