#include <iostream>
#include <Eigen/Dense>
using Eigen::MatrixXd;

MatrixXd generateK() 
{
  MatrixXd m(4,4);
  m(0,0) = 2./3; m(0,1) = -1./6; m(0,2) = -1./3; m(0,3) = -1./6;
  m(1,0) = -1./6; m(1,1) = 2./3; m(1,2) = -1./6; m(1,3) = -1./3;
  m(2,0) =-1./3; m(2,1) =-1./6; m(2,2) =2./3; m(2,3) =-1./6;
  m(3,0) =-1./6; m(3,1) =-1./3; m(3,2) =-1./6; m(3,3) =2./3;
  return m;
};

MatrixXd calculateTemperature(){
MatrixXd T = MatrixXd::Constant(40,1,1.5);
return T;
};

double objective(MatrixXd K, MatrixXd T, MatrixXd x) {
 int n1,n2;
 double c = 0.0;
 int nelx = T.size();
 int nely = T.cols();
 for (int ely = 0; ely < nely; ely++) {
    for (int elx = 0; elx < nelx; elx++) {
	n1 = (nely+1)*(elx-1)+ely;
	n2 = (nely+1)* elx   +ely;
	MatrixXd Ue(4,1);	
	/*Ue << T(n1,1), T(n2,1), T(n2+1,1), T(n1+1,1);*/
	std::cout << Ue << std::endl;
	/*c = c + Ue.transpose()*K*Ue; */
 	/*c = c + (0.001+0.999*x(ely,elx)^3	*/
    };
 };
 return c;
};

int main()
{
  MatrixXd K = generateK();
  MatrixXd T = calculateTemperature();
  MatrixXd x = MatrixXd::Constant(40,1,.4);
  double c = objective(K, T, x);
  std::cout << c << std::endl;
  std::cout << K << std::endl;
};





