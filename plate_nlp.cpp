// Copyright (C) 2005, 2006 International Business Machines and others.
// All Rights Reserved.
// This code is published under the Eclipse Public License.
//
// $Id: plate_nlp.cpp 2005 2011-06-06 12:55:16Z stefan $
//
// Authors:  Carl Laird, Andreas Waechter     IBM    2005-08-16

#include "plate_nlp.hpp"
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <cassert>
#include <iostream>
#include <fstream>

#define K_PLASTIC 0.1
#define K_METAL   80.0

using namespace Ipopt;

typedef Eigen::SparseMatrix<double> SpMat;

Eigen::VectorXd _T;

void PLATE_NLP::finite(const Number* x){

  Eigen::MatrixXd k(_n-1,_n*2-1);
  Eigen::Map<const Eigen::MatrixXd> t(x,k.rows(),k.cols());
  k = t;
  k = (K_PLASTIC + k.array().pow(_penal)*(K_METAL-K_PLASTIC)).matrix();

  int m = 2*_n*_n;
  Eigen::VectorXd F = Eigen::VectorXd::Constant(m,2.5/m);             // the right hand side-vector resulting from the constraints

  SpMat A(m,m);
  A.reserve(Eigen::VectorXi::Constant(m,9));
  int i,j;
  for(j = 0; j < 2*_n; j++){ // cols
    for(i = 0; i < _n; i++){ // rows
      if(j*_n+i >= m-_n/3) // break last third of last row
        break;
      double k0 = 0, k1 = 0, k2 = 0, k3 = 0;
      if(i != 0    && j != 0)
        k0 = k(i-1,j-1);
      if(i != _n-1 && j != 0)
        k1 = k(i,j-1);
      if(i != 0    && j != 2*_n-1)
        k2 = k(i-1,j);
      if(i != _n-1 && j != 2*_n-1)
        k3 = k(i,j);
    
      if(i != 0     && j != 0)
        A.insert((j-1)*_n  +i-1, j*_n+i) = -1.0/3*k0;
      if(j != 0)
        A.insert((j-1)*_n  +i  , j*_n+i) = -1.0/6*(k0+k1);
      if(i != _n-1  && j != 0)
        A.insert((j-1)*_n  +i+1, j*_n+i) = -1.0/3*k1;
      if(i != 0)
        A.insert(j*_n      +i-1, j*_n+i) = -1.0/6*(k0+k2);

      A.insert(  j*_n      +i  , j*_n+i) =  2.0/3*(k0+k1+k2+k3);
      if(i != _n-1)
        A.insert(j*_n      +i+1, j*_n+i) = -1.0/6*(k1+k3);
      if(i != 0    && j != 2*_n-1)
        A.insert((j+1)*_n  +i-1, j*_n+i) = -1.0/3*k2;
      if(j != 2*_n-1)
        A.insert((j+1)*_n  +i  , j*_n+i) = -1.0/6*(k2+k3);
      if(i != _n-1 && j != 2*_n-1)
        A.insert((j+1)*_n  +i+1, j*_n+i) = -1.0/3*k3;
    }
  }
  //Dirichlet
  double T0 = 300;
  j = 2*_n-1;
  for (; i < _n; i++){
    double k0 = 0, k1 = 0;
    k0 = k(i-1,j-1);
    if(i != _n-1)
      k1 = k(i,j-1);

    F((j-1)*_n  +i-1) += 1.0/3*k0*T0;
    F((j-1)*_n  +i  ) += 1.0/6*(k0+k1)*T0;
    F(j*_n      +i-1) += 1.0/6*k0*T0;

    A.coeffRef(j*_n+i,(j-1)*_n  +i-1) = 0;
    A.coeffRef(j*_n+i,(j-1)*_n  +i  ) = 0;
    A.coeffRef(j*_n+i,j*_n      +i-1) = 0;
    A.insert(  j*_n+i,j*_n      +i  ) = 1;

    if(i != _n-1){

    F((j-1)*_n  +i+1) += 1.0/3*k1*T0;
    F(j*_n      +i+1) += 1.0/6*k1*T0;
    A.coeffRef(j*_n+i,(j-1)*_n  +i+1) = 0;
    A.coeffRef(j*_n+i,j*_n      +i+1) = 0;

    }
  }
  F.tail(_n/3).setConstant(T0);
  

  // Solving:
  Eigen::SimplicialLDLT<SpMat> chol(A);  // performs a Cholesky factorization of A
  _T = chol.solve(F);     // use the factorization to solve for the given right hand side
}

// constructor
PLATE_NLP::PLATE_NLP(double volumefrac, int n, double penal)
:_volumefrac(volumefrac),_n(n),_penal(penal)
{
  _T = Eigen::VectorXd(2*n*n);
  Ke << 2.0/3, -1.0/6, -1.0/3, -1.0/6,
       -1.0/6,  2.0/3, -1.0/6, -1.0/3,
       -1.0/3, -1.0/6,  2.0/3, -1.0/6,
       -1.0/6, -1.0/3, -1.0/6,  2.0/3;
}



//destructor
PLATE_NLP::~PLATE_NLP()
{}

// returns the size of the problem
bool PLATE_NLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                             Index& nnz_h_lag, IndexStyleEnum& index_style)
{
  // n is the number of vertices on the left small side, the amount of desity nodes as such is;
  n = (_n-1)*(_n*2-1);

  // one equality constraint;
  m = 1;

  // in this example the jacobian is dense and contains no non-zeros
  nnz_jac_g = n; //Equal to n value

  // the hessian is also dense and has 16 total nonzeros, but we
  // only need the lower left corner (since it is symmetric)
  nnz_h_lag = 0; //All values drop cause of sum.

  // use the C style indexing (0-based)
  index_style = TNLP::C_STYLE;

  return true;
}

// returns the variable bounds
bool PLATE_NLP::get_bounds_info(Index n, Number* x_l, Number* x_u,
                                Index m, Number* g_l, Number* g_u)
{
  // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
  // If desired, we could assert to make sure they are what we think they are.
  assert(m == 1);

  // the variables have lower bounds of 0 and upperbounds of 1, as they represent density.
  for (Index i=0; i<n; i++) {
    x_l[i] = 0.0;
    x_u[i] = 1.0;
  }

  // the first constraint g1 has a lower bound of 0, density cannot be lower than that.
  g_l[0] = 0.;
  // upper bound for total density is equal to 0.4, which is the volume fraction.
  g_u[0] = _volumefrac;


  return true;
}

// returns the initial point for the problem
bool PLATE_NLP::get_starting_point(Index n, bool init_x, Number* x,
                                   bool init_z, Number* z_L, Number* z_U,
                                   Index m, bool init_lambda,
                                   Number* lambda)
{
  // Here, we assume we only have starting values for x, if you code
  // your own NLP, you can provide starting values for the dual variables
  // if you wish
  assert(init_x == true);
  assert(init_z == false);
  assert(init_lambda == false);

  // initialize to the given starting point
  for (Index i=0; i<n; i++) {
    x[i] = _volumefrac;
  }

  return true;
}

// returns the value of the objective function
bool PLATE_NLP::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
{
  //if (new_x)
    finite(x);
  int i,j;
  double vp;
  obj_value = 0;
  Eigen::Vector4d Te;
  for(j = 0; j < 2*_n-1; j++){
    for(i = 0; i < _n-1; i++){
      Te(0) = _T(    j*_n + i);
      Te(1) = _T(    j*_n + i+1);
      Te(2) = _T((j+1)*_n + i+1);
      Te(3) = _T((j+1)*_n + i);
      vp = Te.transpose()*Ke*Te;
      obj_value += (K_PLASTIC + (K_METAL-K_PLASTIC)*pow(x[j*(_n-1)+i],_penal))*vp;
    }
  }

  return true;
}

// return the gradient of the objective function grad_{x} f(x)
bool PLATE_NLP::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
{
  //if (new_x)
    finite(x);
  int i,j;
  double vp;
  Eigen::Vector4d Te;
  for(j = 0; j < 2*_n-1; j++){
    for(i = 0; i < _n-1; i++){
      Te(0) = _T(    j*_n + i);
      Te(1) = _T(    j*_n + i+1);
      Te(2) = _T((j+1)*_n + i+1);
      Te(3) = _T((j+1)*_n + i);
      vp = Te.transpose()*Ke*Te;
      grad_f[j*(_n-1)+i] = -_penal*(K_METAL-K_PLASTIC)*pow(x[j*(_n-1)+i],_penal-1)*vp;
    }
  }
  return true;
}

// return the value of the constraints: g(x)
bool PLATE_NLP::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
{
  assert(m == 1);
  int i;
  g[0] = 0;
  for (i = 0; i < n; i++)
     {
	g[0] += x[i];
     }
  g[0] = g[0]/n;
  return true;
}

// return the structure or values of the jacobian
bool PLATE_NLP::eval_jac_g(Index n, const Number* x, bool new_x,
                           Index m, Index nele_jac, Index* iRow, Index *jCol,
                           Number* values)
{
  int i;
  if (values == NULL) {
    // return the structure of the jacobian

    // this particular jacobian is dense
    for (i = 0; i < n; i++) {
      iRow[i] = 0;
      jCol[i] = i;
    }
  }
  else {
    // return the values of the jacobian of the constraints
    for (i = 0; i < n; i++) {
      values[i] = 1.0/n;
    }
  }

  return true;
}

//return the structure or values of the hessian
bool PLATE_NLP::eval_h(Index n, const Number* x, bool new_x,
                       Number obj_factor, Index m, const Number* lambda,
                       bool new_lambda, Index nele_hess, Index* iRow,
                       Index* jCol, Number* values)
{
  if (values == NULL) {
    // return the structure. This is a symmetric matrix, fill the lower left
    // triangle only.

    // the hessian for this problem is actually dense
    Index idx=0;
    for (Index row = 0; row < 4; row++) {
      for (Index col = 0; col <= row; col++) {
        iRow[idx] = row;
        jCol[idx] = col;
        idx++;
      }
    }

    assert(idx == nele_hess);
  }
  else {
    // return the values. This is a symmetric matrix, fill the lower left
    // triangle only

    // fill the objective portion
    values[0] = obj_factor * (2*x[3]); // 0,0

    values[1] = obj_factor * (x[3]);   // 1,0
    values[2] = 0.;                    // 1,1

    values[3] = obj_factor * (x[3]);   // 2,0
    values[4] = 0.;                    // 2,1
    values[5] = 0.;                    // 2,2

    values[6] = obj_factor * (2*x[0] + x[1] + x[2]); // 3,0
    values[7] = obj_factor * (x[0]);                 // 3,1
    values[8] = obj_factor * (x[0]);                 // 3,2
    values[9] = 0.;                                  // 3,3


    // add the portion for the first constraint
    values[1] += lambda[0] * (x[2] * x[3]); // 1,0

    values[3] += lambda[0] * (x[1] * x[3]); // 2,0
    values[4] += lambda[0] * (x[0] * x[3]); // 2,1

    values[6] += lambda[0] * (x[1] * x[2]); // 3,0
    values[7] += lambda[0] * (x[0] * x[2]); // 3,1
    values[8] += lambda[0] * (x[0] * x[1]); // 3,2

    // add the portion for the second constraint
    values[0] += lambda[1] * 2; // 0,0

    values[2] += lambda[1] * 2; // 1,1

    values[5] += lambda[1] * 2; // 2,2

    values[9] += lambda[1] * 2; // 3,3
  }

  return true;
}

void PLATE_NLP::finalize_solution(SolverReturn status,
                                  Index n, const Number* x, const Number* z_L, const Number* z_U,
                                  Index m, const Number* g, const Number* lambda,
                                  Number obj_value,
				  const IpoptData* ip_data,
				  IpoptCalculatedQuantities* ip_cq)
{
  // here is where we would store the solution to variables, or write to a file, etc
  // so we could use the solution.

  // For this example, we write the solution to the console
  std::cout << std::endl << std::endl << "Solution of the primal variables, x" << std::endl;
  Eigen::Map<const Eigen::MatrixXd> t(x,_n-1,_n*2-1);
  std::ofstream datafile;
  datafile.open("plate_main.out");
  datafile << t;
  datafile.close();

  /*
  std::cout << std::endl << std::endl << "Solution of the bound multipliers, z_L and z_U" << std::endl;
  for (Index i=0; i<n; i++) {
    std::cout << "z_L[" << i << "] = " << z_L[i] << std::endl;
  }
  for (Index i=0; i<n; i++) {
    std::cout << "z_U[" << i << "] = " << z_U[i] << std::endl;
  }*/
  std::cout << std::endl << std::endl << "Final temperature" << std::endl;
  datafile.open("plate_temp.out");
  datafile << Eigen::Map<const Eigen::MatrixXd>(_T.data(),_n,_n*2);
  datafile.close();

  std::cout << std::endl << std::endl << "Objective value" << std::endl;
  std::cout << "f(x*) = " << obj_value << std::endl;

  std::cout << std::endl << "Final value of the constraints:" << std::endl;
  for (Index i=0; i<m ;i++) {
    std::cout << "g(" << i << ") = " << g[i] << std::endl;
  }
}
