#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>

typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double

int main(int argc, char** argv)
{
  assert(argc == 2);
  int n = atoi(argv[1]);// number of elements per row
  int m = n*n/2;      // number of elements

 /* K0 << 2/3, -1/6, -1/3, -1/6
       -1/6,  2/3, -1/6, -1/3
       -1/3, -1/6,  2/3, -1/6
       -1/6, -1/3, -1/6,  2/3; */

  Eigen::ArrayXd rho = Eigen::ArrayXd::Constant((n-1)*(n/2-1),0.4);
  rho = (0.1 + (80-0.1)*pow(rho,3));

  Eigen::Map<Eigen::MatrixXd> k(rho.data(),n/2-1,n-1);
  /*std::cout << k << std::endl;*/

  Eigen::VectorXd F = Eigen::VectorXd::Constant(m,2.5/m);             // the right hand side-vector resulting from the constraints

  SpMat A(m,m);
  A.reserve(Eigen::VectorXi::Constant(m,9));
  int i,j;
  for(j = 0; j < n; j++){ // cols
    for(i = 0; i < n/2; i++){ // rows
      if(j*n/2+i >= m-n/6) // break last third of last row
        break;
      double k0 = 0, k1 = 0, k2 = 0, k3 = 0;
      if(i != 0     && j != 0)
        k0 = k(i-1,j-1);
      if(i != n/2-1 && j != 0)
        k1 = k(i,j-1);
      if(i != 0     && j != n-1)
        k2 = k(i-1,j);
      if(i != n/2-1 && j != n-1)
        k3 = k(i,j);
    
      if(i != 0     && j != 0)
        A.insert((j-1)*n/2  +i-1, j*n/2+i) = -1.0/3*k0;
      if(j != 0)
        A.insert((j-1)*n/2  +i  , j*n/2+i) = -1.0/6*(k0+k1);
      if(i != n/2-1 && j != 0)
        A.insert((j-1)*n/2  +i+1, j*n/2+i) = -1.0/3*k1;
      if(i != 0)
        A.insert(j*n/2      +i-1, j*n/2+i) = -1.0/6*(k0+k2);

      A.insert(  j*n/2      +i  , j*n/2+i) =  2.0/3*(k0+k1+k2+k3);
      if(i != n/2-1)
        A.insert(j*n/2      +i+1, j*n/2+i) = -1.0/6*(k1+k3);
      if(i != 0     && j != n-1)
        A.insert((j+1)*n/2  +i-1, j*n/2+i) = -1.0/3*k2;
      if(j != n-1)
        A.insert((j+1)*n/2  +i  , j*n/2+i) = -1.0/6*(k2+k3);
      if(i != n/2-1 && j != n-1)
        A.insert((j+1)*n/2  +i+1, j*n/2+i) = -1.0/3*k3;
    }
  }
  //Dirichlet
  double T0 = 0;
  j = n-1;
  for (; i < n/2; i++){
    double k0 = 0, k1 = 0;
    k0 = k(i-1,j-1);
    if(i != n/2-1)
      k1 = k(i,j-1);

    F((j-1)*n/2  +i-1) += 1.0/3*k0*T0;
    F((j-1)*n/2  +i  ) += 1.0/6*(k0+k1)*T0;
    F((j-1)*n/2  +i+1) += 1.0/3*k1*T0;
    F(j*n/2      +i-1) += 1.0/6*k0*T0;

    A.insert(  j*n/2+i,j*n/2       +i   ) = 1;
    A.coeffRef(j*n/2+i,((j-1)*n/2  +i+1)) = 0;
    A.coeffRef(j*n/2+i,((j-1)*n/2  +i  )) = 0;
    A.coeffRef(j*n/2+i,((j-1)*n/2  +i-1)) = 0;
    A.coeffRef(j*n/2+i,(j*n/2      +i-1)) = 0;

    if(i != n/2-1){
      F(j*n/2    +i+1) += 1.0/6*k1*T0;
      A.coeffRef(j*n/2+1,(j*n/2    +i+1)) = 0;
    }
  }
  F.tail(n/6).setConstant(T0);
  

  // Solving:
  Eigen::SimplicialLDLT<SpMat> chol(A);  // performs a Cholesky factorization of A
  Eigen::VectorXd T = chol.solve(F);     // use the factorization to solve for the given right hand side
  
  Eigen::Map<Eigen::MatrixXd> Tm(T.data(),n/2,n);
  std::ofstream myfile;
  myfile.open("main.out");
  myfile << Tm ;
  myfile.close();



  // Export the result to a file:
  //saveAsBitmap(x, n, argv[1]);
  return 0;
}

